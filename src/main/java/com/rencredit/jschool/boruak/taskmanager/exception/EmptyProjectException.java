package com.rencredit.jschool.boruak.taskmanager.exception;

public class EmptyProjectException extends RuntimeException {

    public EmptyProjectException() {
        super("Error! Project not exist...");
    }

}
