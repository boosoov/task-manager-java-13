package com.rencredit.jschool.boruak.taskmanager.exception;

public class EmptyDescriptionException extends RuntimeException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}
