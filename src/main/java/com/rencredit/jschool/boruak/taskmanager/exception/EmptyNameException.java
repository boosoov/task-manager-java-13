package com.rencredit.jschool.boruak.taskmanager.exception;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
