package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.exception.*;
import com.rencredit.jschool.boruak.taskmanager.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new EmptyProjectException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new EmptyProjectException();
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        if (project == null) throw new EmptyProjectException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new EmptyProjectException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(id);
    }

}
