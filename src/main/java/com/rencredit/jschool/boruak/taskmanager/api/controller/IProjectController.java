package com.rencredit.jschool.boruak.taskmanager.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

}
