package com.rencredit.jschool.boruak.taskmanager.exception;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
